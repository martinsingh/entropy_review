function calculate_streamfunction_modlev(varargin)
%
% Takes model-level streamfunction data and calculates an estimate of the
% streamfunction in pressure and isentropic coordinates.
%
%

%% Parameters

% Year we want to take an average for
year_in = 1981;
if nargin > 0; year_in = varargin{1}; end

% vars we need for this calculation
vars       = {'va'        'ps'};
realms     = {'atmos'   'atmos'};
data_types = {'ml'        'sfc'};

% The bins for averaging
sigma_edges = [0:0.02:0.08 0.1:0.05:0.9 0.92:0.02:1];
th_edges = [200 225 250:2.5:400 425:25:500];
the_edges = [200 225 250:2.5:400 425:25:500];

sigma_cent = (sigma_edges(1:end-1)+sigma_edges(2:end))./2;
th_cent = (the_edges(1:end-1)+the_edges(2:end))./2;
the_cent = (the_edges(1:end-1)+the_edges(2:end))./2;

c = atm.load_constants;
d = earth.const;


%% Set up the variables

% Set the time vector
freq = '6hr';
time_vec = datenum(year_in(1),1,1):0.25:datenum(year_in+1,1,1)-0.25;
I_vec = zeros(size(time_vec));

% Get the axes
axes = NCI.ERA.get_ERAI_axes;

% Get the pressure edges
p_edges = axes.lev.a_interface + axes.lev.b_interface.*101300;
p_edges = [p_edges; 105000];    
p_cent = (p_edges(1:end-1)+p_edges(2:end))./2;

% Make latitude matrix
lat_mat = repmat(axes.lat',[length(axes.lon) 1 length(axes.lev.num)]);

% Make matrices of a and b
amod = permute(repmat(axes.lev.a_model_ave,[1 length(axes.lon) length(axes.lat)]),[2 3 1]);
bmod = permute(repmat(axes.lev.b_model_ave,[1 length(axes.lon) length(axes.lat)]),[2 3 1]);

aint = permute(repmat(axes.lev.a_interface,[1 length(axes.lon) length(axes.lat)]),[2 3 1]);
bint = permute(repmat(axes.lev.b_interface,[1 length(axes.lon) length(axes.lat)]),[2 3 1]);



% Initialise structures
files = struct;
times = struct;


% Get the files we require
for i_var = 1:length(vars)
  [files.(vars{i_var}),times.(vars{i_var})] = NCI.ERA.get_ERAI_files(vars{i_var},realms{i_var},data_types{i_var},freq);
end

% Get the files in the time range and check they are consistent
for i_var = 1:length(vars)

    % Get the times in range
    I = times.(vars{i_var})(:,2)>=time_vec(1) & times.(vars{i_var})(:,1)<=time_vec(end);
    times.(vars{i_var}) = times.(vars{i_var})(I,:);

    files.(vars{i_var}) = {files.(vars{i_var}){I}};
   
    if i_var > 1
        if length(files.(vars{i_var})) ~= length(files.(vars{1}))
            error('file error')
        end
        
        if ~isequal(times.(vars{i_var}),times.(vars{1}))
            error('time error')
        end
    end
   
end

times = times.(vars{1});


%% Initialise the output variables

% Number of samples for each average
Nsample = zeros(12,1);

% Mass fluxes through model levels
Mflux_lev = zeros(12,length(axes.lat),length(axes.lev.num));


for i_file = 1:length(files.(vars{1}))
    %% Read the time from each file
    
    % Check the file timeseries are consistent
    serial_time = struct;
    for i_var = 1:length(vars)
       [gregorian_time, serial_time.(vars{i_var})] = ncutil.time(files.(vars{i_var}){i_file});
       
       % Check the time is consistent
       if i_var > 1
           if ~isequal(serial_time.(vars{i_var}),serial_time.(vars{1}))
               error(['time error in file: ' files.(vars{i_var}){i_file}])
           end
       end
       
      
    end
    
    serial_time = serial_time.(vars{1});
    
    
    
    %% Loop over each sample in the file
    for i_ind = 1:length(serial_time)
        tic

        % Disp some info to screen
        disp(datestr(serial_time(i_ind)))
        
        % Check if current sample is in range required
        current_day = abs(serial_time(i_ind)-time_vec)<1/24;
        if sum(current_day) == 1
        
                        
            i_month = month(serial_time(i_ind));
            
            
            Nsample(i_month) = Nsample(i_month)+1;
            I_vec(current_day) = I_vec(current_day) +1;
        
            
            
            % Load the data
            for i_var = 1:length(vars)
               var.(vars{i_var}) = NCI.ERA.get_field(files.(vars{i_var}){i_file},i_ind,vars{i_var});
            end
            
        
        
            
            %% Do the calculation
            
            % Calculate pressure
            p = amod + bmod.*var.ps;
          
            % Calculate the pressure thickness
            dp = diff(aint + bint.*var.ps,1,3);
            
            % Calculate the mass flux in each layer
            Mflux = var.va.*dp.*2.*pi().*d.Re.*cosd(lat_mat)./c.g;
            
            % Now go through all the layers and sum the mass fluxes
            Mflux_lev(i_month,:,:) = squeeze(Mflux_lev(i_month,:,:)) + squeeze(mean(Mflux));
           
            
        end
        
    end
end
 

lat = axes.lat;
lon = axes.lon;

for i = 1:12
   Mflux_lev(i,:,:) = Mflux_lev(i,:,:)./Nsample(i);
end

Streamfunction_lev = cumsum(Mflux_lev,3);

Streamfunction_lev = cat(3,zeros(12,length(lat),1),Streamfunction_lev);


save(['../mat_data/ERAI/mass_fluxes_lev_' num2str(year_in) '.mat'],...
    'Mflux_lev',...
    'Streamfunction_lev',...
    'axes',...
    'lat','lon',...
    'I_vec','Nsample','time_vec');

        
