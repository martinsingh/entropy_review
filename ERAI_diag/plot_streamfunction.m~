% plot streamfunction

years = [1981:1981];

%% Average the streamfunction over the appropriate years

for i = years

   load(['../mat_data/ERAI/mass_fluxes_' num2str(i) '.mat'])
    
   % Delete this later
   Streamfunction_p = cumsum(Mflux_p,3);
   Streamfunction_th = cumsum(Mflux_th,3);
   Streamfunction_the = cumsum(Mflux_the,3);
   
   % Delete this later
   Streamfunction_p = cat(3,zeros([12,length(lat),1]),Streamfunction_p);
   Streamfunction_th = cat(3,zeros([12,length(lat),1]),Streamfunction_th);
   Streamfunction_the = cat(3,zeros([12,length(lat),1]),Streamfunction_the);
   
   if i == years(1)
       Psi_p = zeros(length(lat),length(p_edges));
       Psi_th = zeros(length(lat),length(th_edges));
       Psi_the = zeros(length(lat),length(the_edges));
   end
   
   for j = 1:12
      fact = Nsample(j)./sum(Nsample)./length(years);
      
      Psi_p = Psi_p + fact.*squeeze(Streamfunction_p(j,:,:));
      Psi_th = Psi_th - fact.*squeeze(Streamfunction_th(j,:,:));
      Psi_the = Psi_the - fact.*squeeze(Streamfunction_the(j,:,:));
   end
   
end

%% Calculate the maxima for each case
[Pmax_p,Imax] = max(Psi_p(:));
[Pmin_p,Imin] = min(Psi_p(:));
[Imax_p,Jmax_p] = ind2sub(size(Psi_p),Imax);
[Imin_p,Jmin_p] = ind2sub(size(Psi_p),Imin);


[Pmax_th,Imax] = max(Psi_th(:));
[Pmin_th,Imin] = min(Psi_th(:));
[Imax_th,Jmax_th] = ind2sub(size(Psi_th),Imax);
[Imin_th,Jmin_th] = ind2sub(size(Psi_th),Imin);


[Pmax_the,Imax] = max(Psi_the(:));
[Pmin_the,Imin] = min(Psi_the(:));
[Imax_the,Jmax_the] = ind2sub(size(Psi_the),Imax);
[Imin_the,Jmin_the] = ind2sub(size(Psi_the),Imin);


%% Make the figure

fig.bfig(7,11)
fig.set_default('paper')
set(gcf,'defaultaxesfontsize',7)
set(gcf,'defaultaxeslinewidth',1)

L = 0.17;
B = [0.08 0.4 0.72];
W = 0.8;
H = 0.25;

dPsi = 1.5e10;


ax=axes('position',[L B(3) W H])

contour(lat,p_edges./100,Psi_p',dPsi:dPsi:20*dPsi,'k')
hold on
contour(lat,p_edges./100,Psi_p',-20*dPsi:dPsi:-dPsi,'color',[0.5 0.5 0.5])

plot(lat(Imax_p),p_edges(Jmax_p)./100,'.k')
plot(lat(Imin_p),p_edges(Jmin_p)./100,'.k','color',[0.5 0.5 0.5])

text(35,90,num2str(round(Pmax_p./1e9)),'fontsize',7,'horizontalalignment','center')
text(-35,90,num2str(round(-Pmin_p./1e9)),'fontsize',7,'horizontalalignment','center')

line([lat(Imax_p) 35 35],[p_edges(Jmax_p)./100 240 140],'color','k')
line([lat(Imin_p) -35 -35],[p_edges(Jmin_p)./100 240 140],'color','k')



ylabel('pressure (hPa)')
set(gca,'ydir','reverse')
set(gca,'xlim',[-85 85])
set(gca,'ylim',[30 1030],'ytick',[200 400 600 800 1000])
box off
set(gca,'xtick',[-60 -30 0 30 60])

fig.format_ticks(ax,'\circ{}','')

text(-82,50,'a','fontsize',9,'fontweight','bold')




ax = axes('position',[L B(2) W H]);

contour(lat,th_edges,Psi_th',dPsi:dPsi:20*dPsi,'k')
hold on
contour(lat,th_edges,Psi_th',-20*dPsi:dPsi:-dPsi,'color',[0.5 0.5 0.5])
set(gca,'xlim',[-85 85],'ylim',[260 370])
box off
ylabel('potential temp. (K)')

set(gca,'xtick',[-60 -30 0 30 60])
set(gca,'ytick',[270 300 330 360])
fig.format_ticks(ax,'\circ{}','')

plot(lat(Imax_th),th_edges(Jmax_th),'.k')
plot(lat(Imin_th),th_edges(Jmin_th),'.k','color',[0.5 0.5 0.5])

line([lat(Imax_th) 35 35],[th_edges(Jmax_th) 360 380],'color','k')
line([lat(Imin_th) -35 -35],[th_edges(Jmin_th) 360 380],'color','k')

text(35,390,num2str(round(Pmax_th./1e9)),'fontsize',7,'horizontalalignment','center')
text(-35,390,num2str(round(-Pmin_th./1e9)),'fontsize',7,'horizontalalignment','center')

text(-82,395,'b','fontsize',9,'fontweight','bold')





ax = axes('position',[L B(1) W H])

contour(lat,the_edges,Psi_the',dPsi:dPsi:20*dPsi,'k')
hold on
contour(lat,the_edges,Psi_the',-20*dPsi:dPsi:-dPsi,'color',[0.5 0.5 0.5])
set(gca,'xlim',[-85 85],'ylim',[260 370])
box off
set(gca,'xtick',[-60 -30 0 30 60])
set(gca,'ytick',[270 300 330 360])
ylabel('equiv. potential temp. (K)')

fig.format_ticks(ax,'\circ{}','')

xx=xlabel('latitude')
xxpos = get(xx,'position');
set(xx,'position',xxpos-[0 15 0])

plot(lat(Imax_the),th_edges(Jmax_the),'.k')
plot(lat(Imin_the),th_edges(Jmin_the),'.k','color',[0.5 0.5 0.5])

line([lat(Imax_the) 55 55],[the_edges(Jmax_the) 340 360],'color','k')
line([lat(Imin_the) -55 -55],[the_edges(Jmin_the) 340 380],'color','k')


text(55,360,num2str(round(Pmax_the./1e9)),'fontsize',7,'horizontalalignment','center')
text(-55,360,num2str(round(-Pmin_the./1e9)),'fontsize',7,'horizontalalignment','center')

text(-82,365,'c','fontsize',9,'fontweight','bold')

print -dpdf ../Figures/streamfunction.pdf






























