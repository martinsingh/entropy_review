function calculate_streamfunction(varargin)
%
% Takes model-level streamfunction data and calculates an estimate of the
% streamfunction in pressure and isentropic coordinates.
%
%

%% Parameters

% Year we want to take an average for
year_in = 1981;
if nargin > 0; year_in = varargin{1}; end

% vars we need for this calculation
vars       = {'va'    'ta'    'hus'    'ps'};
realms     = {'atmos' 'atmos' 'atmos'  'atmos'};
data_types = {'ml'    'ml'    'ml'     'sfc'};

% The bins for averaging
sigma_edges = [0:0.02:0.08 0.1:0.05:0.9 0.92:0.02:1];
th_edges = [200 225 250:2.5:400 425:25:500];
the_edges = [200 225 250:2.5:400 425:25:500];

sigma_cent = (sigma_edges(1:end-1)+sigma_edges(2:end))./2;
th_cent = (the_edges(1:end-1)+the_edges(2:end))./2;
the_cent = (the_edges(1:end-1)+the_edges(2:end))./2;

c = atm.load_constants;
d = earth.const;


%% Set up the variables

% Set the time vector
freq = '6hr';
time_vec = datenum(year_in(1),1,1):0.25:datenum(year_in+1,1,1)-0.25;
I_vec = zeros(size(time_vec));

% Get the axes
axes = NCI.ERA.get_ERAI_axes;

% Get the pressure edges
p_edges = axes.lev.a_interface + axes.lev.b_interface.*101300;
p_edges = [p_edges; 105000];    
p_cent = (p_edges(1:end-1)+p_edges(2:end))./2;

% Make latitude matrix
lat_mat = repmat(axes.lat',[length(axes.lon) 1 length(axes.lev.num)]);

% Make matrices of a and b
amod = permute(repmat(axes.lev.a_model_ave,[1 length(axes.lon) length(axes.lat)]),[2 3 1]);
bmod = permute(repmat(axes.lev.b_model_ave,[1 length(axes.lon) length(axes.lat)]),[2 3 1]);

aint = permute(repmat(axes.lev.a_interface,[1 length(axes.lon) length(axes.lat)]),[2 3 1]);
bint = permute(repmat(axes.lev.b_interface,[1 length(axes.lon) length(axes.lat)]),[2 3 1]);



% Initialise structures
files = struct;
times = struct;


% Get the files we require
for i_var = 1:length(vars)
  [files.(vars{i_var}),times.(vars{i_var})] = NCI.ERA.get_ERAI_files(vars{i_var},realms{i_var},data_types{i_var},freq);
end

% Get the files in the time range and check they are consistent
for i_var = 1:length(vars)

    % Get the times in range
    I = times.(vars{i_var})(:,2)>=time_vec(1) & times.(vars{i_var})(:,1)<=time_vec(end);
    times.(vars{i_var}) = times.(vars{i_var})(I,:);

    files.(vars{i_var}) = {files.(vars{i_var}){I}};
   
    if i_var > 1
        if length(files.(vars{i_var})) ~= length(files.(vars{1}))
            error('file error')
        end
        
        if ~isequal(times.(vars{i_var}),times.(vars{1}))
            error('time error')
        end
    end
   
end

times = times.(vars{1});


%% Initialise the output variables

% Number of samples for each average
Nsample = zeros(12,1);

% Mass fluxes through model levels
Mflux_p = zeros(12,length(axes.lat),length(p_edges)-1);
Mflux_th = zeros(12,length(axes.lat),length(th_edges)-1);
Mflux_the = zeros(12,length(axes.lat),length(the_edges)-1);
Mflux_sigma = zeros(12,length(axes.lat),length(sigma_edges)-1);

% Mean thermodynamic variables
T_mean = zeros(12,length(axes.lat),length(axes.lev.num));
p_mean = zeros(12,length(axes.lat),length(axes.lev.num));
sigma_mean = zeros(12,length(axes.lat),length(axes.lev.num));
rv_mean = zeros(12,length(axes.lat),length(axes.lev.num));

theta_mean = zeros(12,length(axes.lat),length(axes.lev.num));
thetae_mean = zeros(12,length(axes.lat),length(axes.lev.num));



for i_file = 1:length(files.(vars{1}))
    %% Read the time from each file
    
    % Check the file timeseries are consistent
    serial_time = struct;
    for i_var = 1:length(vars)
       [gregorian_time, serial_time.(vars{i_var})] = ncutil.time(files.(vars{i_var}){i_file});
       
       % Check the time is consistent
       if i_var > 1
           if ~isequal(serial_time.(vars{i_var}),serial_time.(vars{1}))
               error(['time error in file: ' files.(vars{i_var}){i_file}])
           end
       end
       
      
    end
    
    serial_time = serial_time.(vars{1});
    
    
    
    %% Loop over each sample in the file
    for i_ind = 1:length(serial_time)
        tic

        % Disp some info to screen
        disp(datestr(serial_time(i_ind)))
        
        % Check if current sample is in range required
        current_day = abs(serial_time(i_ind)-time_vec)<1/24;
        if sum(current_day) == 1
        
                        
            i_month = month(serial_time(i_ind));
            
            
            Nsample(i_month) = Nsample(i_month)+1;
            I_vec(current_day) = I_vec(current_day) +1;
        
            
            
            % Load the data
            for i_var = 1:length(vars)
               var.(vars{i_var}) = NCI.ERA.get_field(files.(vars{i_var}){i_file},i_ind,vars{i_var});
            end
            
        
        
            
            %% Do the calculation
            
            % Calculate pressure
            p = amod + bmod.*var.ps;
            
            % Calculate sigma
            sigma = p./var.ps;
            
            % Calculate potential temperature
            theta = var.ta ./ ( p./ c.p00 ).^(c.Rd./c.cp);
            
            % Calculate mixing ratio
            rv = var.hus./(1-var.hus);
            
            % Calculate equivalent potential temperature
            s = atm.calculate_entropy(var.ta,p,rv);
            theta_e = c.T0.*exp(s./c.cp);
            
            % Calculate the pressure thickness
            dp = diff(aint + bint.*var.ps,1,3);
            
            % Calculate the mass flux in each layer
            Mflux = var.va.*dp.*2.*pi().*d.Re.*cosd(lat_mat)./c.g;
            
            
            % Calculate some mean values
            
            T_mean(i_month,:,:) = squeeze(T_mean(i_month,:,:)) + squeeze(mean(var.ta));
            p_mean(i_month,:,:) = squeeze(p_mean(i_month,:,:)) + squeeze(mean(p));
            sigma_mean(i_month,:,:) = squeeze(sigma_mean(i_month,:,:)) + squeeze(mean(sigma));
            rv_mean(i_month,:,:) = squeeze(rv_mean(i_month,:,:)) + squeeze(mean(rv));
            
            theta_mean(i_month,:,:) = squeeze(theta_mean(i_month,:,:)) + squeeze(mean(theta));
            thetae_mean(i_month,:,:) = squeeze(thetae_mean(i_month,:,:)) + squeeze(mean(theta_e));
            
            
            
            
            
            % Now go through all the layers and sum the mass fluxes

            for i = 1:length(p_edges)-1
                I = p>=p_edges(i) & p<p_edges(i+1);
                Mflux_p(i_month,:,i) = Mflux_p(i_month,:,i) + sum(squeeze(mean(Mflux.*I)),2)';
            end
            
            for i = 1:length(th_edges)-1
                I = theta>=th_edges(i) & theta<th_edges(i+1);
                Mflux_th(i_month,:,i) = Mflux_th(i_month,:,i) + sum(squeeze(mean(Mflux.*I)),2)';
            end
            
            for i = 1:length(the_edges)-1
                I = theta_e>=the_edges(i) & theta_e<the_edges(i+1);
                Mflux_the(i_month,:,i) = Mflux_the(i_month,:,i) + sum(squeeze(mean(Mflux.*I)),2)';
            end
  
            for i = 1:length(sigma_edges)-1
                I = sigma>=sigma_edges(i) & sigma<sigma_edges(i+1);
                Mflux_sigma(i_month,:,i) = Mflux_sigma(i_month,:,i) + sum(squeeze(mean(Mflux.*I)),2)';
            end
            
            
        end
        
    end
end
 

lat = axes.lat;
lon = axes.lon;

for i = 1:12
   Mflux_p(i,:,:) = Mflux_p(i,:,:)./Nsample(i);
   Mflux_sigma(i,:,:) = Mflux_sigma(i,:,:)./Nsample(i);
   Mflux_th(i,:,:) = Mflux_th(i,:,:)./Nsample(i);
   Mflux_the(i,:,:) = Mflux_the(i,:,:)./Nsample(i);
   
   T_mean(i,:,:) = T_mean(i,:,:)./Nsample(i);
   p_mean(i,:,:) = p_mean(i,:,:)./Nsample(i);
   sigma_mean(i,:,:) = sigma_mean(i,:,:)./Nsample(i);
   rv_mean(i,:,:) = rv_mean(i,:,:)./Nsample(i);
   
   theta_mean(i,:,:) = theta_mean(i,:,:)./Nsample(i);
   thetae_mean(i,:,:) = thetae_mean(i,:,:)./Nsample(i);
end

Streamfunction_p = cumsum(Mflux_p,3);
Streamfunction_sigma = cumsum(Mflux_sigma,3);
Streamfunction_th = cumsum(Mflux_th,3);
Streamfunction_the = cumsum(Mflux_the,3);

Streamfunction_p = cat(3,zeros(12,length(lat),1),Streamfunction_p);
Streamfunction_sigma = cat(3,zeros(12,length(lat),1),Streamfunction_sigma);
Streamfunction_th = cat(3,zeros(12,length(lat),1),Streamfunction_th);
Streamfunction_the = cat(3,zeros(12,length(lat),1),Streamfunction_the);



save(['../mat_data/ERAI/mass_fluxes_' num2str(year_in) '.mat'],...
    'Mflux_p','Mflux_th','Mflux_the','Mflux_sigma',...
    'Streamfunction_p','Streamfunction_th','Streamfunction_the','Streamfunction_sigma',...
    'T_mean','p_mean','rv_mean','theta_mean','thetae_mean',...
    'th_edges','the_edges','p_edges','th_cent','the_cent','p_cent',...
    'lat','lon',...
    'I_vec','Nsample','time_vec');

        
