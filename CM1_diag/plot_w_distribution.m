% plot the vertical velocity snapshots from dry and moist RCE

%% The parameters
klev = 19;


%% The parameters
c = atm.load_constants;

dat = CM1_data;
simulation = dat.dry;
dry = load(['../mat_data/w_dist_' simulation '.mat']);

simulation = dat.moist;
moist = load(['../mat_data/w_dist_' simulation '.mat']);

z = moist.z;


%% Plot the velocity distribution
fig.set_default('paper')
fig.bfig(8,6)

set(gcf,'defaultaxeslinewidth',0.5)

w_pdf_dry = dry.w_hist(klev,:)./diff(dry.w_bin_ends)./sum(dry.w_hist(klev,:));
plot(dry.w_bins,w_pdf_dry,'-','color',[0.5 0.5 0.5],'linewidth',1)

hold on

w_pdf_moist = moist.w_hist(klev,:)./diff(moist.w_bin_ends)./sum(moist.w_hist(klev,:));
plot(moist.w_bins,w_pdf_moist,'k-','linewidth',1)

plot([0 0],[1e-10 100],'k:')

set(gca,'yscale','log','xlim',[-10 10],'ylim',[1e-4 10])
set(gca,'ticklength',[0.02 0.02],'yminortick','on')
xlabel('vertical velocity (m s^{-1})')
ylabel('PDF (s m^{-1})')
box off

text(3,0.1,'dry','color',[0.5 0.5 0.5],'fontsize',8)
text(2,0.002,'moist','fontsize',8)

print -dpdf ../Figures/w_pdf.pdf



