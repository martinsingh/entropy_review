%% Save the w distribution

c = CM1_data;

simulation = c.moist;


data_prefix = [c.data_loc '/' simulation '/netcdf/' ];



Ns = c.snapshots(2)-c.snapshots(1)+1;
nt = 0;
for i = 1:Ns
    
    
    
   disp(['t = ' num2str(i)])
   is = i-1+c.snapshots(1);
    
   filei = [data_prefix 'cm1out_' num2str(is,'%06i') '.nc'];
   
   if i == 1
       x = ncread(filei,'xh');
       y = ncread(filei,'yh');
       zi = ncread(filei,'zf');
       z = (zi(2:end)+zi(1:end-1))./2;
       

       T_mean = zeros(length(z),1);
       qv_mean = zeros(length(z),1);
       p_mean = zeros(length(z),1);
       
   end
   
   th = ncread(filei,'th');
   pi = ncread(filei,'pi');
   p = ncread(filei,'prs');
   if contains(simulation,'dry')
       qv = zeros(size(p));
   else
      qv = ncread(filei,'qv');
   end
   
   T = th.*pi;
   
   T_mean = T_mean + squeeze(mean(mean(T)));
   p_mean = p_mean + squeeze(mean(mean(p)));
   qv_mean = qv_mean + squeeze(mean(mean(qv)));
   
   
   nt = nt+1;
end

% Calculae means
T_mean = T_mean./nt;
p_mean = p_mean./nt;
qv_mean = qv_mean./nt;


save(['../mat_data/mean_fields_' simulation '.mat'],'x','y','z','zi',...
    'T_mean','qv_mean','p_mean')




