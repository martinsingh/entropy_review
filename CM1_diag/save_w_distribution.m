%% Save the w distribution

c = CM1_data;

simulation = c.moist;
%c.snapshots = [9 19];


percentiles = [0.00001 0.0001 0.001 0.005:0.005:0.995 0.999 0.9999 0.99999];

w_bin_ends = -4.5:0.1:4.5;
w_bin_ends = w_bin_ends.*abs(w_bin_ends);
w_bins = (w_bin_ends(2:end)+w_bin_ends(1:end-1))./2;
w_vec = -20:0.1:20;

data_prefix = [c.data_loc '/' simulation '/netcdf/' ];

Ns = c.snapshots(2)-c.snapshots(1)+1;
for i = 1:Ns
    
    
    
   disp(['t = ' num2str(i)])
   is = i-1+c.snapshots(1);
    
   filei = [data_prefix 'cm1out_' num2str(is,'%06i') '.nc'];
   
   if i == 1
       x = ncread(filei,'xh');
       y = ncread(filei,'yh');
       zi = ncread(filei,'zf');
       z = (zi(2:end)+zi(1:end-1))./2;
       w_mat = zeros(length(zi),length(x),length(y),Ns);
   end
   
   w = ncread(filei,'w');
   w_mat(:,:,:,i) =permute(w,[3 1 2]);
    
    
end

% Calculae CDF, histogram and estimate of PDF
w_cdf = zeros(length(zi),length(percentiles));
w_pdf = zeros(length(zi),length(w_vec));
w_hist = zeros(length(zi),length(w_bins));

for i = 2:length(zi)-1
    disp(['lev = ' num2str(i)])
    wi = w_mat(i,:,:,:);
    
    w_cdf(i,:) = quantile(wi(:),percentiles);
    
    [w_pdf(i,:),xi,bw(i)] = ksdensity(wi(:),w_vec);
    
    ww = histc(wi(:),w_bin_ends);
    w_hist(i,:) = ww(1:end-1);
    
end


save(['../mat_data/w_dist_' simulation '.mat'],'x','y','z','zi','w_bin_ends','w_bins','w_vec','percentiles',...
    'w_hist','w_cdf','w_pdf','bw')




