% Simple one-layer entropy model
%
% Assume sun, Earth and atmosphere have a single temperature and act like
% black bodies for emission
%
% Atmosphere is transparent to shortwave and Earth is a Lambertian
% reflector with a spectrally indepenfent albedo

%% Parameters

% Emission temperature
Te = 255;

% Surface temprature
Ts = 288;

% albedo
alpha = 0.3;

% Fraction of sphere covered by solar disk
% Taken from Stephens & Obrien
Sf = 67.7e-6./pi;

% Average value of cos(zenith angle)
cosphi = 0.25;

% Load the Stefan-Boltzmann constant
c = atm.load_constants;

% Function associated with Lambertian reflection
% Taken from Stephens & O'Brien
p = -0.2777;
q = 0.9652;
Chi = @(u)  u.*(p.*log(u) + q);

%% Calculate energy balance 

% Calculate the solar blackbody temperature based on TOA energy balance
Tsun = (1./(cosphi.*Sf.*(1-alpha)))^0.25.*Te;



%% Calculate the Entropy fluxes

% TOA long-wave entropy flux
Jt_L = 4./3.*c.sigma.*Te.^3;

% Downward TOA solar entropy flux
Jt_Sd = 4./3.*c.sigma.*Tsun.^3.*cosphi.*Sf;

% Upward TOA solar entropy flux
Jt_Su = 4./3.*c.sigma.*Tsun.^3.*Chi(alpha.*cosphi.*Sf);

% Planetary entropy production
Sdot_p  = Jt_L + Jt_Su - Jt_Sd;


%% Calculate Material entropy production

% Heating of the atmosphere by radiation
Q = c.sigma.*(Ts.^4 - 2.*Te.^4);

% Material entropy production
Sdot_mat = Q.*(1./Ts - 1./Te);

% Carnot efficiency according to Bannon 2015
etaC = (Ts-Te)./Ts;


%% Calculate transfer entropy production

% Absorption at the surface/cooling to space
Q = -c.sigma.*Te.^4;

Sdot_trans = Q.*(1./Ts - 1./Te);


%% Print output

disp('--- Single-layer atmosphere model ---')
disp(' ')
disp('TOA entropy fluxes (outward positive):')
disp(['J_Ln = ' num2str(Jt_L.*1000) ' mW m^-2'])
disp(['J_Sn = ' num2str((Jt_Su-Jt_Sd).*1000) ' mW m^-2'])
disp(['  J_Su = ' num2str(Jt_Su.*1000) ' mW m^-2'])
disp(['  J_Sd = ' num2str(-Jt_Sd.*1000) ' mW m^-2'])
disp(' ')
disp(['Planetary entropy production: ' num2str(Sdot_p.*1000) ' mW m^-2'])
disp(['Material  entropy production: ' num2str(Sdot_mat.*1000) ' mW m^-2'])
disp(['Transfer  entropy production: ' num2str(Sdot_trans.*1000) ' mW m^-2'])



