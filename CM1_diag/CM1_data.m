function c = CM1_data


c.data_loc = '/g/data/k10/mss565/CM1_output';

c.dry = 'SST301.54-N192_dx500.0_newtrad_dry';
c.moist = 'SST301.54-N192_dx500.0_newtrad_surf';

c.snapshots = [20 259];



