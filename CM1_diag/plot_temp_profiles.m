% plot the vertical velocity snapshots from dry and moist RCE

%% The parameters
c = atm.load_constants;

dat = CM1_data;
simulation = dat.dry;
dry = load(['../mat_data/mean_fields_' simulation '.mat']);

simulation = dat.moist;
moist = load(['../mat_data/mean_fields_' simulation '.mat']);

z = moist.z;

%% Calculate the adiabat
Ts = moist.T_mean(1);
rs = moist.qv_mean(1);
ps = moist.p_mean(1);
zs = 50;


p = (ps:-500:5000)';
zr = zeros(size(p));
zp = zeros(size(p));

[Tp,rv,rl,ri,T_rhop] = atm.calculate_adiabat(Ts,rs,ps,p,1);
[Tr,rv,rl,ri,T_rhor] = atm.calculate_adiabat(Ts,rs,ps,p,0,'default',1,0);

% dz/dlogp = -Rd.*T_rho/g


zr(1) = zs;
zp(1) = zs;
for i = 1:length(p)-1
    
    dlogp = log(p(i+1)) - log(p(i));
    
    dzdlnp = @(logp,z) -(c.Rd.*T_rhor(i))./c.g;
    zr(i+1) = ODE.rk_step(dzdlnp,zr(i),log(p(i)),dlogp);
    
    dzdlnp = @(logp,z) -(c.Rd.*T_rhop(i))./c.g;
    zp(i+1) = ODE.rk_step(dzdlnp,zp(i),log(p(i)),dlogp);
    
    
end

%% Plot the temperature profiles
fig.set_default('paper')
fig.bfig(8,6)

plot(dry.T_mean,z,'-','color',[0.5 0.5 0.5],'linewidth',1)
hold on;
plot(moist.T_mean,z,'-k','linewidth',1);

plot(Tp,zp./1000,'k:')

set(gca,'xlim',[180 300],'ylim',[0 17])
box off
xlabel('temperature (K)')
ylabel('height (km)')

text(222,6,'dry','fontsize',8,'color',[0.5 0.5 0.5])
text(247,6,'moist','fontsize',8)


print -dpdf ../Figures/T_profiles.pdf

%% some extra analysis stuff

SH_dry = 67;
LH_moist = 106;
SH_moist = 15;

% find the tropopause
dTdz = gradient(dry.T_mean,z);
I = find(dTdz>-2,1);
z_trop_dry = interp1(dTdz(I-1:I),z(I-1:I),-2);

dTdz = gradient(moist.T_mean,z);
I = find(dTdz>-2,1);
z_trop_moist = interp1(dTdz(I-1:I),z(I-1:I),-2);

%% Make a larger plot with some extra stuff


fig.set_default('paper')
fig.bfig(14,6)
set(gcf,'defaultaxeslinewidth',1);


L = [0.08 0.32 0.72];
B = 0.15;
W1 = 0.2;
W2 = 0.36;
H = 0.78;

axes('position',[L(2) B W2 H])
plot(dry.T_mean,z,'-','color',[0.5 0.5 0.5],'linewidth',1)
hold on;
plot(moist.T_mean,z,'-k','linewidth',1);

plot(Tp,zp./1000,'k:')

set(gca,'xlim',[185 300],'ylim',[0 17])

box off
xlabel('temperature (K)')
set(gca,'ycolor',[1 1 0.999])

%ylabel('height (km)')



axes('position',[L(1) B W1 H])


dx = 0.02;
annotation('line',[L(1)+2*dx L(1)+W1],[B B])
plot([L(1)+2*dx,L(1)+W1],[z_trop_dry z_trop_dry],'color',[0.65 0.65 0.65],'linewidth',4)

set(gca,'xcolor',[1 1 0.999])
set(gca,'xlim',[L(1) L(1)+W1],'ylim',[0 17])
set(gca,'ytick',[0 3 6 9 12 15])
ylabel('height (km)')
box off

L0 = L(1)+2*dx;

for i = 1:floor(W1/dx)-2
   annotation('line',[L0+dx L0],[B B-dx])
   L0 = L0+dx;
end

%text(L(1)+dx/2+W1/2,8,'$Q = -\frac{(T-T_0)}{\tau}$','interpreter','latex','horizontalalignment','center','fontsize',8)

%text(L(1)+dx/2+W1/2,-2,'$T_s = 300$ K','interpreter','latex','horizontalalignment','center','fontsize',8)


fig.arrow([L(1)+dx*4,0],[L(1)+dx*4,SH_dry./30],'width',3,'length',8,'tipangle',25,'color',[0.5 0.5 0.5])
%text(L(1)+dx*2,1.5,'$F_{SH}$','interpreter','latex','horizontalalignment','center','fontsize',8)
text(L(1)+dx*4,SH_dry./30+1,[num2str(SH_dry) ],'horizontalalignment','center','fontsize',8)


text(L(1)+W1/2+dx/2,-2,'dry','fontsize',8,'horizontalalignment','center')


axes('position',[L(3) B W1 H])


dx = 0.02;
annotation('line',[L(3) L(3)+W1-2*dx],[B B])
plot([L(3),L(3)+W1-2*dx],[z_trop_moist z_trop_moist],'-','color',[0.5 0.5 0.5],'linewidth',4)

set(gca,'xcolor',[1 1 0.999])
set(gca,'xlim',[L(3) L(3)+W1],'ylim',[0 17])
ylabel('height (km)')
set(gca,'ytick',[0 3 6 9 12 15])
box off
set(gca,'yaxislocation','right')

L0 = L(3);

for i = 1:floor(W1/dx)-2
   annotation('line',[L0+dx L0],[B B-dx])
   L0 = L0+dx;
end

%text(L(3)+dx/2+W1/2,10,'$Q = -\frac{(T-T_0)}{\tau}$','interpreter','latex','horizontalalignment','center','fontsize',8)

%text(L(3)+dx/2+W1/2,-2,'$T_s = 300$ K','interpreter','latex','horizontalalignment','center','fontsize',8)


fig.arrow([L(3)+dx*3,0],[L(3)+dx*3,SH_moist./30],'width',3,'length',4,'tipangle',25,'color',[0.65 0.65 0.65])
text(L(3)+dx*3,SH_moist./30+1,[num2str(SH_moist)],'horizontalalignment','center','fontsize',8)


fig.arrow([L(3)+W1-dx*4,0],[L(3)+W1-dx*4,LH_moist./30],'width',3,'length',8,'tipangle',25)
text(L(3)+W1-dx*4,LH_moist./30+1,[num2str(LH_moist)],'horizontalalignment','center','fontsize',8)


text(L(3)+W1/2-dx,-2,'moist','fontsize',8,'horizontalalignment','center')


print -dpdf ../Figures/RCE.pdf






