% plot the vertical velocity snapshots from dry and moist RCE

%% The parameters

klev = 19; % about 4 km
is = 19; % 90 days into simulation

c = CM1_data;

simulation = c.dry;



%% Load the data

% Dry simulation
data_prefix = [c.data_loc '/' c.dry '/netcdf/' ];
fdry = [data_prefix 'cm1out_' num2str(is,'%06i') '.nc'];
   
% Moist simulation
data_prefix = [c.data_loc '/' c.moist '/netcdf/' ];
fmoist = [data_prefix 'cm1out_' num2str(is,'%06i') '.nc'];

% Load the axes
ncutil.ncload(fdry,'xh','yh','zf');
zh = (zf(2:end)+zf(1:end-1))./2;

% Load the velocities
w_dry = ncread(fdry,'w');
w_moist = ncread(fmoist,'w');

% Load the cloud
qc = ncread(fmoist,'qc');
qi = ncread(fmoist,'qi');
qcl = qc+qi;
qcl = qcl(:,:,klev);

% Load the humidity
r = ncread(fmoist,'qv');
r = squeeze(mean(mean(r)));

% Load the pressure
p = ncread(fmoist,'prs');
p = squeeze(mean(mean(p)));


% Load the temperatures
th = ncread(fdry,'th');
pi = ncread(fdry,'pi');
T_dry = squeeze(mean(mean(th.*pi)));

th = ncread(fmoist,'th');
pi = ncread(fmoist,'pi');
T_moist = squeeze(mean(mean(th.*pi)));


%% Plot the snapshot of w

FW = 12;
FH = 8;
fig.bfig(FW,FH)
set(gcf,'defaultaxeslinewidth',1)

L = [0.09 0.52];
B = 0.28;
W = 0.4;
H = W.*FW./FH;

axes('position',[L(1) B W H])


contourf(xh,yh,w_dry(:,:,klev),101,'linestyle','none')

%contour(xh,yh,w_dry(:,:,klev),[3 3],'r')
%hold on
%contour(xh,yh,w_dry(:,:,klev),[1 1],'y')
%contour(xh,yh,w_dry(:,:,klev),[-1 -1],'c')
%contour(xh,yh,w_dry(:,:,klev),[-3 -3],'b')

set(gca,'clim',[-4 4])
fig.red2blue

set(gca,'xtick',[20 40 60 80],'ytick',[20 40 60 80],'ticklength',[0.02 0.02],'xminortick','on','yminortick','on')
xlabel('x (km)')
ylabel('y (km)')

t = text(2,92,'a','fontweight','bold','backgroundcolor','w')
text(48,104,'dry','horizontalalignment','center','fontsize',10)


axes('position',[L(2) B W H])

contourf(xh,yh,w_moist(:,:,klev),101,'linestyle','none')

%contour(xh,yh,w_moist(:,:,klev),[3 3],'r')
%hold on
%contour(xh,yh,w_moist(:,:,klev),[1 1],'y')
%contour(xh,yh,w_moist(:,:,klev),[-1 -1],'c')
%contour(xh,yh,w_moist(:,:,klev),[-3 -3],'b')

hold on
contour(xh,yh,qcl,[1e-5 1e-5],'k')

set(gca,'clim',[-4 4])
fig.red2blue

set(gca,'xtick',[20 40 60 80],'ytick',[20 40 60 80],'ticklength',[0.02 0.02],'xminortick','on','yminortick','on')
set(gca,'yticklabel','')
xlabel('x (km)')

t = text(2,92,'b','fontweight','bold','backgroundcolor','w')
text(48,104,'moist','horizontalalignment','center','fontsize',10)

c = colorbar('location','southoutside');
set(c,'position',[0.3 0.12 0.4 0.025])
xlabel(c,'vertical velocity (m s^{-1})','fontsize',8)


print -r600 -dpng ../Figures/w_snapshot.png

return







%% Plot the velocity distribution
fig.set_default('paper')
fig.bfig(8,6)

set(gcf,'defaultaxeslinewidth',0.5)


plot(w_bins,w_dist_dry,'-','color',[0.5 0.5 0.5],'linewidth',1)
hold on
plot(w_bins,w_dist_moist,'k-','linewidth',1)
set(gca,'yscale','log','xlim',[-8 8],'ylim',[1e-4 10])
set(gca,'ticklength',[0.02 0.02],'yminortick','on')
xlabel('vertical velocity (m s^{-1})')
ylabel('PDF (s m^{-1})')
box off

text(3,0.1,'dry','color',[0.5 0.5 0.5],'fontsize',8)
text(2,0.002,'moist','fontsize',8)

print -dpdf ../figures/w_pdf.pdf



%% Calculate the adiabat
Ts = T_moist(1);
rs = r(1);
ps = p(1);
zs = 0;


p = (ps:-500:5000)';
zr = zeros(size(p));
zp = zeros(size(p));

[Tp,rv,rl,ri,T_rhop] = atm.calculate_adiabat(Ts,rs,ps,p,1);
[Tr,rv,rl,ri,T_rhor] = atm.calculate_adiabat(Ts,rs,ps,p,0,'default',1,0);

% dz/dlogp = -Rd.*T_rho/g


zr(1) = zh(1).*1000;
zp(1) = zh(1).*1000;
for i = 1:length(p)-1
    
    dlogp = log(p(i+1)) - log(p(i));
    
    dzdlnp = @(logp,z) -(c.Rd.*T_rhor(i))./c.g;
    zr(i+1) = ODE.rk_step(dzdlnp,zr(i),log(p(i)),dlogp);
    
    dzdlnp = @(logp,z) -(c.Rd.*T_rhop(i))./c.g;
    zp(i+1) = ODE.rk_step(dzdlnp,zp(i),log(p(i)),dlogp);
    
    
end

%% Plot the temperature profiles
fig.set_default('paper')
fig.bfig(8,6)

plot(T_dry,zh,'-','color',[0.5 0.5 0.5],'linewidth',1)
hold on;
plot(T_moist,zh,'-k','linewidth',1);

plot(Tp,zp./1000,'k:')

set(gca,'xlim',[200 300],'ylim',[0 12.5])
box off
xlabel('temperature (K)')
ylabel('height (km)')

text(214,6,'dry','fontsize',8,'color',[0.5 0.5 0.5])
text(238,6,'moist','fontsize',8)


print -dpdf ../Figures/T_profiles.pdf

